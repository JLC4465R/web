
const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
var fs = require("fs");
var util = require('util');
const session =  require('express-session') ;
/*
var jwt = require('jsonwebtoken');
const morgan = require('morgan');
const _ = require('lodash');
var sdk = require("microsoft-cognitiveservices-speech-sdk");
var fs = require("fs");
var util = require('util');

var subscriptionKey = "11207e6e366e4dd6aed126f88cd359cd";
var serviceRegion = "francecentral";

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
// conexion para Atlas (Azure)
//var connectionStr = 'mongodb://sts:p0DOpHouWyK3OmOiludVN6bc8Z9iTNzt9rrJMqAvFNDtdwsnIA9JvBCK69lWpcr8TFNpR7NV1ETRNFwaU4qNWw==@sts.documents.azure.com:10255/?ssl=true&replicaSet=globaldb'
// Conexión para AWS
var connectionStr="mongodb://adminMD:Jleyva3001@docdb-2020-06-13-09-56-34.cluster-cebqtsvqe5b2.eu-west-3.docdb.amazonaws.com:27017/?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false";
const session =  require('express-session') ;


var validateTokenJWT= function(token){
  
 
 try
 {
  if(!token){
      return 1
  }
  
  
  token = token.replace('Bearer ', '')

  return jwt.verify(token, 'jleyva', function(err, user) {
    if (err) {
      console.log("\r\n Error validate token:"+err+"\r\n");
      return 1;
    } else {
      console.log("\r\n Validate token: OK\r\n");
      return 0;
    }
  })

  } catch (err) {
    console.log("\r\n Error validate token:"+err+"\r\n");
    return 1;
}

};
var getQuery=  function(document,res) {


  MongoClient.connect(connectionStr, function(err, client) {
      assert.equal(null, err);
      const db = client.db('StsDB');
     
      //Step 1: declare promise
     
      var myPromise = async () => {
        return new Promise((resolve, reject) => {
       
           db
            .collection('STS')
            .findOne({"_id":new ObjectId(document._id)},function(err, result) {
               assert.equal(err, null);
               console.log("\r\n Id Audio:"+document._id+":Timne:"+Math.floor(Date.now()/1000)+":"+"Find a document into the STS collection: "+document._id);
               client.close();
               resolve(result);
               
              });
              
           });
           
        };
     
     
      //Step 2: async promise handler
      var GetDBRecord =  async () =>{    
         var result = await  myPromise();
        
         //anything here is executed after result is resolved
         console.log("\r\n Id Audio:"+document._id+":Timne:"+Math.floor(Date.now()/1000)+":"+"Response Query");
         res.send({
          status: result.status,
          message: "OK",
          data: {
            "_id":result._id,
            "id": result.id,
            "mimetype": result.mimetype,
            "size": result.size,
            "language":result.language,
            "date":result.date,
            "lastupdate":result.lastupdate,
            "STT": result.STT
          }});
         return result;
      };

      //Step 3: make the call
      
      var result=   GetDBRecord();
   
      console.log("\r\nId Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"GetDBRecord: "+document._id);
      
        
    }); //end mongo client

   
  };


var updateDBRecord = function(MongoClient,document) {

  
  MongoClient.connect(connectionStr, function(err, client) {
      assert.equal(null, err);
      const db = client.db('StsDB');
      
      db.collection('STS').updateOne(
        { "_id" : ObjectId(document._id)},
        {
          $set: { "STT": document.STT,"lastupdate": Math.floor(Date.now()/1000),"status":document.status}
        }, function(err, results) {
     
          console.log("\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"updateDBRecord: "+document._id+"STT : "+document.STT);
          client.close();
        });
  });

};     
var executeProcessSTS=  function(document) {


  MongoClient.connect(connectionStr, function(err, client) {
      assert.equal(null, err);
      const db = client.db('StsDB');
     
      //Step 1: declare promise
     
      var myPromise = async () => {
        return new Promise((resolve, reject) => {
       
           db
            .collection('STS')
            .insertOne( document,function(err, result) {
               assert.equal(err, null);
               console.log("\r\n Id Audio:"+document._id+":Timne:"+Math.floor(Date.now()/1000)+":"+"Inserted a document into the STS collection: "+document._id);
               client.close();
               resolve(result);
               
              });
              
           });
           
        };
     
     
      //Step 2: async promise handler
      var InsertDBRecord =  async () =>{    
         var result = await  myPromise();
        
         //anything here is executed after result is resolved
         console.log("\r\n Id Audio:"+document._id+":Timne:"+Math.floor(Date.now()/1000)+":"+"ExecuteSTS");
         executeSTS(document);
         return result;
      };

      //Step 3: make the call
      
      var result=   InsertDBRecord();
   
      console.log("\r\nId Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"InsertDBRecord: "+document._id);
      
        
    }); //end mongo client

   
  };


  var executeSTS = function(document) {
try {
    var pushStream = sdk.AudioInputStream.createPushStream();

    // open the file and push it to the push stream.
    fs.createReadStream('./'+document._id+'/'+document.id).on('data', function(arrayBuffer) {
      pushStream.write(arrayBuffer.slice());
    }).on('end', function() {
      pushStream.close();
    });
    

    // now create the audio-config pointing to our stream and
    // the speech config specifying the language.
    var audioConfig = sdk.AudioConfig.fromStreamInput(pushStream);
    //audioConfig.expiresIn=10000000000;
    var speechConfig = sdk.SpeechConfig.fromSubscription(subscriptionKey, serviceRegion);

    // setting the recognition language to English.
    speechConfig.speechRecognitionLanguage = document.language;
    //speechConfig.enableDictation();
    // create the speech recognizer.
    var reco = new sdk.SpeechRecognizer(speechConfig, audioConfig);
    reco.setTimeout=600000;
    // Before beginning speech recognition, setup the callbacks to be invoked when an event occurs.

    // The event recognizing signals that an intermediate recognition result is received.
    // You will receive one or more recognizing events as a speech phrase is recognized, with each containing
    // more recognized speech. The event will contain the text for the recognition since the last phrase was recognized.
    reco.recognizing = function (s, e) {
      //var str = "(recognizing) Reason: " + sdk.ResultReason[e.result.reason] + " Text: " + e.result.text;
      //console.log(str);
    };

  // The event recognized signals that a final recognition result is received.
  // This is the final event that a phrase has been recognized.
  // For continuous recognition, you will get one recognized event for each phrase recognized.
  reco.recognized = function (s, e) {
      // Indicates that recognizable speech was not detected, and that recognition is done.
      if (e.result.reason === sdk.ResultReason.NoMatch) {
          var noMatchDetail = sdk.NoMatchDetails.fromResult(e.result);
          //console.log("\r\n(recognized)  Reason: " + sdk.ResultReason[e.result.reason] + " NoMatchReason: " + sdk.NoMatchReason[noMatchDetail.reason]);
      } else {
        console.log("\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"(recognized)  Reason: " + sdk.ResultReason[e.result.reason] + " Text: " + e.result.text);
        document.STT=document.STT+e.result.text;
      }
  };

  // The event signals that the service has stopped processing speech.
  // https://docs.microsoft.com/javascript/api/microsoft-cognitiveservices-speech-sdk/speechrecognitioncanceledeventargs?view=azure-node-latest
  // This can happen for two broad classes of reasons.
  // 1. An error is encountered.
  //    In this case the .errorDetails property will contain a textual representation of the error.
  // 2. Speech was detected to have ended.
  //    This can be caused by the end of the specified file being reached, or ~20 seconds of silence from a microphone input.
  reco.canceled = function (s, e) {
      var str = "(cancel) Reason: " + sdk.CancellationReason[e.reason];
      if (e.reason === sdk.CancellationReason.Error) {
          str += ": " + e.errorDetails;
      }
      console.log(str);
    };

  // Signals that a new session has started with the speech service
  reco.sessionStarted = function (s, e) {
      var str = "\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"(sessionStarted) SessionId: " + e.sessionId;
      console.log(str);
      
  };

  // Signals the end of a session with the speech service.
  reco.sessionStopped = function (s, e) {
      var str = "\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"(sessionStopped) SessionId: " + e.sessionId;
      console.log(str);
  };

  // Signals that the speech service has started to detect speech.
  reco.speechStartDetected = function (s, e) {
    var str = "\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"(sessionStartDetected) SessionId: " + e.sessionId;
    console.log(str);
  };
  
  
  // Signals that the speech service has detected that speech has stopped.
  reco.speechEndDetected = function (s, e) {
    var str = "\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+"(sessionEndDetected) SessionId: " + e.sessionId;
    console.log(str);
  
    document.status=0;

    updateDBRecord(MongoClient,document);     
    reco.close();
    reco = undefined;
  };
  
    // start the recognizer and wait for a result.
    
    //reco.startContinuousRecognitionAsync();
    //reco.expiresIn=1000000000000;

    var s =function intervalFunc(){
      
      
      console.log("********** :Status de los datos obtenidos/memoria: *******"+":Time:"+Math.floor(Date.now()/1000));
      const used = process.memoryUsage();
      for (let key in used) {
        console.log(`${key} ${Math.round(used[key] /1024 /1024*100) / 100 } MB`);
      };

      reco.stopContinuousRecognitionAsync();
      reco.startContinuousRecognitionAsync();
      setTimeout(s,240000);
      document.status=0;

      updateDBRecord(MongoClient,document);     
      reco.close();
      reco = undefined;
    
    };
    
    reco.startContinuousRecognitionAsync();
  
    //setTimeout(s,240000);
}
catch(e)
{
  console.log("Aqui salta un error:"+e)
}
};
*/
//  App which will accept Rest call
var app = express();

// Enable files upload
app.use(fileUpload({
    createParentPath: true
}));


app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'));


var log_file = fs.createWriteStream('./debug.log', {flags : 'a'});
// Probamos a montar el directorio /home/app/log en el volumen data-volumen
//docker volume inspect data-volume
// A ver para pruebas de montar volumenes
var log_stdout = process.stdout;

console.log = function(d) { 
  log_file.write(util.format(d));
  log_stdout.write(util.format(d));
};

// Base on ObjectID should be posible to get record from CosmoDB 
/*
app.get('/STS', function (req, res) {


  if(!req.header("_id")||validateTokenJWT(req.header("authorization"))) {
    res.send({
        status: false,
        message: 'No audio _id/authorization provide'
    });
  } else {
    try {
      
      
      var document={
        "_id":new ObjectId(req.header("_id"))
      };
      getQuery(document,res);
      
    
      
    } catch (err) {
      console.log(err);
      res.status(500).send(err);
    }
    }
  });
 
  app.get('/STS/Test1', function (req, res) {
   
      try {

       session.username="jleyva";  
           
        res.send("<!DOCTYPE html> \
        <html> \
           <head> \
              <title>App interna (Page1)</title> \
           </head> \
           <body> \
              <h1>App interna acceso a Page2</h1> \
              <a href=\"\\STS\\Test2\\\">Acceso Pag2</a> \
           </body> \
        </html>");
        
      } catch (err) {
        console.log(err);
        res.status(500).send(err);
      }
      
    });
*/
    app.get('/Test', function (req, res) {
      
      
        try {
        
          
          session.username="jleyva";  
          
          res.send("<!DOCTYPE html> \
        <html> \
           <head> \
              <title>HTML Links (Page2)</title> \
           </head> \
           <body> \
              <h1>App interna acceso a Page1</h1> \
              <a href=\"\\STS\\Test1\\\">Acceso a Pag1</a> Obtenemos username session: "+session.username+" \
           </body> \
        </html>");
          console.log("Graba en cada llamada, en disco y pantalla");
        } catch (err) {
          console.log(err);
          res.status(500).send(err);
        }
        
      });
    
/*
      app.post('/iphone', function (req, res) {
      
      
        try {
        
          
       
          
          
          
        } catch (err) {
          console.log(err);
          res.status(500).send(err);
        }
        
      });

app.post('/STS', async (req, res) => {
  try {

      // We have defined a very straighforward way to secure calls (token in header).We have to imporve this mecahnism, perhaps a token JWT

      if(!req.files.avatar||!req.header("language")||validateTokenJWT(req.header("authorization"))) {
          res.send({
              status: false,
              message: 'No audio uploaded/language/authorization provide'
          });
      } else {
          
          var avatar = req.files.avatar;
          let language= req.header("language");
          // Unique identifier associate to hte wav file (CosmoBD and File Share)
          var Id_insert = new ObjectId();
         
          // Use the mv() method to place the file somewhere on your server
          // We will use a data disc (SCSI) mounted in the vm (I have examples of doing this)

          avatar.mv('./'+Id_insert+'/'+ avatar.name , function(err) {
          if(err){
              console.log("\r\n Id Audio:"+Id_insert+":Time:"+Math.floor(Date.now()/1000)+":"+err);
          }else{
              console.log("\r\n Id Audio:"+Id_insert+":Time:"+Math.floor(Date.now()/1000)+":"+"Uploaded audio file");
          }
          });
          
          var document={
            "_id":Id_insert,
            "id": avatar.name,
            "mimetype": avatar.mimetype,
            "size": avatar.size,
            "status": "1",
            "STT":"",
            "language": language,
            "date":Math.floor(Date.now()/1000)
          };

          executeProcessSTS(document);
          
          res.send({
              status: document.status,
              message: "OK",
              data: {
                "_id":document._id,
                "id": document.id,
                "mimetype": document.mimetype,
                "size": document.size,
                "language":document.language,
                "date":document.date
            }
          });
      }
  } catch (err) {
      console.log("\r\n Id Audio:"+document._id+":Time:"+Math.floor(Date.now()/1000)+":"+err)
      res.status(500).send(err);
  }
});

app.get('/callback', (req, res) => {
  console.log("Hola"+req.params);
});

app.post('/login', (req, res) => {
  var token = req.body.token
  var email = req.body.email
  console.log("Token: "+token+"- Email: "+email)
/*
  if( !(username === 'POC' && password === 'STS')){
    res.status(401).send({
      error: 'Invalid Credentials'
    })
    return
  }

  var tokenData = {
    data: "Data"
    // ANY DATA
  }

  var token = jwt.sign(tokenData, 'jleyva', {
     expiresIn: 60 * 60 * 24 // expires in 24 hours
  })
 
//res.redirect("https://www.elmundo.es")
//res.end()
res.redirect(303, 'https://www.elmundo.es');
res.end();

res.send({
    status: 0,
    message: "OK",
    data: {
      "token":token
  }
});
})
*/
app.listen(3000);
