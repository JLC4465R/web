FROM ubuntu:latest
USER root
WORKDIR /home/app
COPY ./package.json /home/app/package.json
COPY ./app.js /home/app/app.js
RUN chmod 777 /home/app
RUN apt-get update
RUN apt-get -y install curl gnupg
RUN apt-get -y install nodejs
RUN apt-get -y  install npm
RUN npm install express express-fileupload cors body-parser morgan fs util express-session
EXPOSE 3000
CMD [ "node", "app.js" ]
